import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;

public class ImageEncryptor {

    // Encryption key in hexadecimal
    private static final String KEY_HEX = "770A8A65DA156D24EE2A093277530142";
    // Path to the BMP image file
    private static final String INPUT_FILE = "D:\\idea\\IntelliJ IDEA 2023.1.4\\daima\\waikato\\aes-encryption\\Image-Assignment2.bmp";
    // Encryption modes to use
    private static final String[] MODES = {"ECB", "CBC", "CFB"};

    public static void main(String[] args) throws Exception {
        // Convert key from hex to byte array
        byte[] key = hexStringToByteArray(KEY_HEX);

        // Load the image
        BufferedImage image = ImageIO.read(new File(INPUT_FILE));
        int width = image.getWidth();
        int height = image.getHeight();

        // Total bytes in the image is RGB 24-bit
        int numBytes = width * height * 3;

        // Get pixel data from the image
        byte[] pixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();

        // Encrypt and save the image under each mode
        for (String mode : MODES) {
            byte[] encryptedBytes = encryptImagePixels(pixels, key, mode, numBytes);
            BufferedImage outputImage = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
            System.arraycopy(encryptedBytes, 0, ((DataBufferByte) outputImage.getRaster().getDataBuffer()).getData(), 0, numBytes);
            File outputFile = new File("encrypted_" + mode.toLowerCase() + ".jpg");
            ImageIO.write(outputImage, "jpg", outputFile);
        }
    }

    // Encrypts image pixel data
    private static byte[] encryptImagePixels(byte[] pixels, byte[] key, String mode, int numBytes) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/" + mode + "/PKCS5Padding");
        SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
        IvParameterSpec ivSpec = new IvParameterSpec(new byte[16]);

        if (!mode.equals("ECB")) {
            cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);
        } else {
            cipher.init(Cipher.ENCRYPT_MODE, keySpec);
        }

        // Make sure the data is a multiple of the block size
        byte[] dataToEncrypt = new byte[(numBytes + 15) / 16 * 16];
        System.arraycopy(pixels, 0, dataToEncrypt, 0, numBytes);

        return cipher.doFinal(dataToEncrypt);
    }

    // Converts hex string to byte array
    private static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }
}
